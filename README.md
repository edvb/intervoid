# NAME

intervoid - A multiplayer space combat game

## SYNOPSIS

intervoid [--help] [--version]

## DESCRIPTION

A multiplayer space combat game where you jump around in zero g to shoot your
opponents. Also try not to jump into the void in the process.

# Keys

Press ESC to quit to main menu and again to close the game
