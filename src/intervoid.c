#include <getopt.h>
#include <stdio.h>

#include "intervoid.h"

static const struct option longopts[] = {
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'},
	{NULL, 0, NULL, 0}
};

static void
iv_help(void) {
	printf("\
Usage: intervoid [OPTION]\n\
A multiplayer space combat game\n\
\n\
  -h, --help      display this help and exit\n\
  -v, --version   display version information and exit\n\
\n\
For more info see man page\n\
\n\
intervoid home page: <https://gitlab.com/edvb/intervoid>\n\
");
}

static void
iv_version(void) {
	printf("intervoid v%s\n", VERSION);
}

int main(int argc, char *argv[]) {
	int optc;

	while ((optc = getopt_long(argc, argv, "hv", longopts, NULL)) != -1)
		switch (optc) {
		case 'h':
			iv_help();
			return 0;
		case 'v':
			iv_version();
			return 0;
		default:
			printf("for help run \"intervoid --help\"\n");
			return 1;
		}

	SDL_Event e;

	if (!iv_init()) return 1;

	game_loop(e);

	iv_cleanup();

}

// vim: set shiftwidth=8 tabstop=8:
