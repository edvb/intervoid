#include "intervoid.h"

char map[MAX_Y/16][MAX_X/16+1] = {
	"        #######                 ",
	"        #     #                 ",
	"        #     #                 ",
	"        #     #                 ",
	"        #     #                 ",
	"        #     #                 ",
	"        #     ########          ",
	"        #           ##          ",
	"        #            #          ",
	"        #     ###### #          ",
	"        #     #    # #          ",
	"        #     ##   # #          ",
	"        #     ###### ####       ",
	"        #         ## #  #       ",
	"        #               #       ",
	"        #               #       ",
	"        #               #       ",
	"        #            ####       ",
	"        #            #          ",
	"        ######    ####          ",
	"           #         #          ",
	"           #         #          ",
	"           #         #          ",
	"           ###########          "
};

/* TODO make init_level load more than one level */
/* init_level: populate Level l with data, right now it is hard coded in */
void init_level(Level *l) {
	l->name = "Pocket Cave";
	l->img = load_img("data/levels/pocket_cave.png");
	for (int i = 0; i < MAX_X; i++)
		for (int j = 0; j < MAX_Y; j++)
			l->data[j][i] = map[j][i];
	l->init_pos[0][0] = 12;
	l->init_pos[0][1] = 18;
	l->init_pos[0][2] = 0;
	l->init_pos[1][0] = 19;
	l->init_pos[1][1] = 22;
	l->init_pos[1][2] = 0;
}

/* get_map: return block char at x and y */
char get_map(int x, int y) {
	return iv_game.level[iv_game.levelnum].data[y][x];
}

// vim: set shiftwidth=8 tabstop=8:
