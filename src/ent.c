#include <math.h>

#include "intervoid.h"

static struct _Keys player_keys[MAX_PLAYERS] = {
{ SDL_SCANCODE_H, SDL_SCANCODE_J, SDL_SCANCODE_K, SDL_SCANCODE_L,
  SDL_SCANCODE_E, SDL_SCANCODE_R, SDL_SCANCODE_T },
{ SDL_SCANCODE_A, SDL_SCANCODE_S, SDL_SCANCODE_W, SDL_SCANCODE_D,
  SDL_SCANCODE_C, SDL_SCANCODE_V, SDL_SCANCODE_B },
};

struct Ent_t {
	char *name;
	int color;
	int speed;
};

static struct Ent_t player_t[MAX_PLAYERS] = {
{ "player1",  3, 1 },
{ "player2",  1, 1 },
};

/* init_bullet: populates an entity as a bullet */
void init_bullet(SDL_Rect dst, int direc) {
	if (entqty >= MAX_ENTS)
		for (int i = 0; i < entqty; i++)
			if (ent[i].type == BULLET)
				entqty = i;

	ent[entqty].name = malloc(MAX_NAME * sizeof(char));
	strcpy(ent[entqty].name, "bullet");
	ent[entqty].type = BULLET;
	ent[entqty].color = 0;

	ent[entqty].img = load_img("data/ents/bullet.png");
	ent[entqty].src = (SDL_Rect){ 6, 6, 8, 4 };
	ent[entqty].dst.x = dst.x + cos(direc*PI/180)*(16) - sin(direc*PI/180)*8;
	ent[entqty].dst.y = dst.y - sin(direc*PI/180)*(16) + cos(direc*PI/180)*8;
	ent[entqty].dst.w = ent[entqty].src.w;
	ent[entqty].dst.h = ent[entqty].src.h;

	ent[entqty].direc = direc;
	ent[entqty].rot = direc;
	ent[entqty].look = 0;
	ent[entqty].speed = 8;

	ent[entqty].isdead = false;
	ent[entqty].isloaded = 0;
	entqty++;
}

/* init_ent: copies values from player_t[] to player[] */
bool init_ent(int count) {
	for (int num = 0; num < count; num++) {
		ent[num].name = malloc(MAX_NAME * sizeof(char));
		strcpy(ent[num].name, player_t[num].name);
		ent[num].type = PLAYER;
		ent[num].color = player_t[num].color;

		char str[64] = {0};
		strcpy(str, "data/ents/");
		strcat(str, ent[num].name);
		strcat(str, ".png");
		ent[num].img = load_img(str);
		ent[num].src = (SDL_Rect) { 0, 0, 16, 16 };
		ent[num].dst = (SDL_Rect) {
			iv_game.level[0].init_pos[num][0]*16,
			iv_game.level[0].init_pos[num][1]*16,
			16, 16
		};

		ent[num].direc = -1;
		ent[num].rot = iv_game.level[iv_game.levelnum].init_pos[num][2];
		ent[num].look = 0;
		ent[num].speed = player_t[num].speed;

		ent[num].isdead = false;
		ent[num].isloaded = 0;

		ent[num].keys = player_keys[num];

	}

	entqty = count;

	return true;
}

/* ent_map_coll: move an entity by dx and dy */
static void
ent_map_coll(Ent *e, int x, int y, int dx, int dy) {
	int s = 16;
	if (get_map((x+dx)/s, y/s) == '#') {
		e->direc = -1;
		e->dst.x -= dx;
		e->dst.y += dx;
		e->rot = 90;
		return;
	} else if (get_map((x+dx+(s-1))/s, y/s) == '#') { /* TODO sin/cos? */
		e->direc = -1;
		/* e->dst.x += dx; */
		e->dst.y -= dx;
		e->rot = 270;
		return;
	} else if (get_map(x/s, (y+dy)/s) == '#') {
		e->direc = -1;
		e->dst.x += dy;
		e->dst.y -= dy;
		e->rot = 180;
		return;
	} else if (get_map(x/s, (y+dy+(s-1))/s)== '#') {
		e->direc = -1;
		e->dst.x -= dy;
		/* e->dst.y += dy; */
		e->rot = 0;
		return;
	}
	e->dst.x += dx;
	e->dst.y += dy;
	return;
}

/* move_ent_jump: move entity depending on direction */
static void
move_ent_jump(Ent *e, int direc) {
	switch (direc) {
	case 0:
		ent_map_coll(e, e->dst.x, e->dst.y, -e->speed*2, 0);
		break;
	case 180:
		ent_map_coll(e, e->dst.x, e->dst.y, e->speed*2, 0);
		break;
	case 90:
		ent_map_coll(e, e->dst.x, e->dst.y, 0, -e->speed*2);
		break;
	case 270:
		ent_map_coll(e, e->dst.x, e->dst.y, 0, e->speed*2);
		break;
	}
}

/* move_ent: move an entity depending on rotation and direction */
static void
move_ent(Ent *e, int direc) {
	switch (e->rot) {
	case 0:
		ent_map_coll(e, e->dst.x, e->dst.y, e->speed*direc, 0);
		break;
	case 180:
		ent_map_coll(e, e->dst.x, e->dst.y, -e->speed*direc, 0);
		break;
	case 90:
		ent_map_coll(e, e->dst.x, e->dst.y, 0, e->speed*direc);
		break;
	case 270:
		ent_map_coll(e, e->dst.x, e->dst.y, 0, -e->speed*direc);
		break;
	}
}

/* change_tex: move selected texture to other textures around the sprite sheet */
void change_tex(Ent *e, int num) {
	e->src.x = num * 16;
	if (e->isloaded > 0)
		e->src.y = 16;
	else
		e->src.y = 0;
}

/* draw entity e to screen, if alive */
void draw_ent(Ent e) {
	if (e.isdead)
		return;
	SDL_Rect dst = { e.dst.x*ZOOM, e.dst.y*ZOOM,
	                 e.dst.w*ZOOM, e.dst.h*ZOOM };
	SDL_RenderCopyEx(iv_ren, e.img, &e.src, &dst, e.rot, NULL, SDL_FLIP_NONE);
}

/* bullet_run: bullet code */
void bullet_run(Ent *e) {
	if (!e->isdead && e->direc != -1) {
		e->dst.x += cos(e->direc*PI/180)*e->speed;
		e->dst.y -= sin(e->direc*PI/180)*e->speed;
		for (int i = 0; i < playerqty; i++)
			if (dst_collision(e->dst, ent[i].dst))
				ent[i].isdead = true;
	}
}

/* player_run: player code */
void player_run(SDL_Event event, Ent *e) {
	const Uint8* k = SDL_GetKeyboardState(NULL);
	if (e->isdead)
		return;
	if (e->isloaded > 0) {
		e->src.y = 16;
		e->isloaded--;
	} else
		e->src.y = 0;
	if (e->direc != -1) {
		add_degrees(&e->rot, 1);
		move_ent_jump(e, e->direc);
		if (k[e->keys.fire] && e->isloaded <= 0) {
			init_bullet(e->dst, e->look-e->rot);
			e->isloaded = 240;
		}
		return;
	}

	if (k[e->keys.left]) {
		move_ent(e, -1);
		change_tex(e, 0);
		e->look = 180;
	} else if (k[e->keys.down]) {
		change_tex(e, 1);
		e->look = 270;
	} else if (k[e->keys.up]) {
		change_tex(e, 2);
		e->look = 90;
	} else if  (k[e->keys.right]) {
		move_ent(e, 1);
		change_tex(e, 3);
		e->look = 0;
	} else if (k[e->keys.fire] && e->isloaded <= 0) {
		init_bullet(e->dst, e->look-e->rot);
		e->isloaded = 240;
	} else if  (k[e->keys.jump]) {
		e->direc = e->rot + 90;
	}
}

// vim: set shiftwidth=8 tabstop=8:
