#include "intervoid.h"

/* TODO actually write this code */

void menu_forward(int sel) {
	switch (sel) {
		case 0:
			if (playerqty != MAX_PLAYERS-1)
				playerqty++;
			break;
		case 1:
		case 2:
			break;
	}
}

void menu_backward(int sel) {
	switch (sel) {
		case 0:
			if (playerqty > 1)
				playerqty--;
			break;
		case 1:
		case 2:
			break;
	}
}

void menu_draw(int sel) {
	/* SDL_Color color = { 255, 255, 255 }; */
}

void menu_loop(SDL_Event e) {
	int sel = 0;

	do {
		SDL_RenderClear(iv_ren);

		const Uint8* k = SDL_GetKeyboardState(NULL);
		if        (k[ent[0].keys.left]) { menu_forward(sel);
		} else if (k[ent[0].keys.down]) { sel++;
		} else if (k[ent[0].keys.up]) {   sel--;
		} else if (k[ent[0].keys.right]) { menu_backward(sel);
		}

		menu_draw(sel);

		SDL_PollEvent(&e);

	} while (e.key.keysym.sym != SDLK_ESCAPE);
}

