#include "intervoid.h"

/* load_img: returns loaded png found at path */
SDL_Texture *load_img(char *path) {
	SDL_Texture *tex = NULL;
	tex = IMG_LoadTexture(iv_ren, path);
	if (tex == NULL) {
		printf("SDL_image Error: %s\n", path);
		return nullimg;
	}

	return tex;
}

/*  */
void draw_text(char *str, SDL_Color color) {
	SDL_Surface *surf = TTF_RenderText_Solid(font, str, color);
	if (surf == NULL) {
		printf("SDL Error: %s\n", SDL_GetError());
		return;
	}
	SDL_Texture *tex = SDL_CreateTextureFromSurface(iv_ren, surf);
	if (tex == NULL) {
		printf("SDL Error: %s\n", SDL_GetError());
		return;
	}
	SDL_FreeSurface(surf);
	SDL_RenderCopy(iv_ren, tex, NULL, &(SDL_Rect){50,0,60*9,80});
	return;
}

/* TODO: Clean up this shit-show */
/* dst_collision: returns true if dst_1 and dst_2 are inside of each other */
bool dst_collision(SDL_Rect dst_1, SDL_Rect dst_2) {
	if ((dst_1.x >= dst_2.x && dst_1.x <= (dst_2.x + dst_2.w)) ||
	    ((dst_1.x + dst_1.w) >= dst_2.x && (dst_1.x + dst_1.w) <= (dst_2.x + dst_2.w)))
		if ((dst_1.y >= dst_2.y && dst_1.y <= (dst_2.y + dst_2.h)) ||
		    ((dst_1.y + dst_1.h) >= dst_2.y && (dst_1.y + dst_1.h) <= (dst_2.y + dst_2.h)) )
			return true;
	return false;
}

/* add_degrees: adds diff to old, checks to make sure it is still in range */
void add_degrees(int *old, int diff) {
	*old += diff;
	if (*old >= 360)
		*old -= 360;
	if (*old < 0)
		*old += 360;
}

// vim: set shiftwidth=8 tabstop=8:
