#ifndef INTERVOID_H
#define INTERVOID_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define PI 3.141592653589793238462643

/* max constants */
#define MAX_X 512
#define MAX_Y 384
#define MAX_NAME 16
#define MAX_PLAYERS 4
#define MAX_ENTS 64
#define MAX_LEVELS 32

#define ZOOM 2

typedef enum { false, true } bool;

typedef enum {
	PLAYER,
	BULLET,
	BOMB
} ENT_TYPE;

/* player keys */
typedef struct _Keys Keys;
struct _Keys {
	Uint8 left, down, up, right;
	Uint8 fire, jump, bomb;
};

typedef struct _Level Level;
struct _Level {
	char *name;
	SDL_Texture *img;
	char data[MAX_Y/16][MAX_X/16+1];
	int init_pos[MAX_PLAYERS][3];
};

typedef struct _Game Game;
struct _Game {
	Level level[MAX_LEVELS];
	int levelnum;
};

/* universal struct for players and other entities */
typedef struct _Ent Ent;
struct _Ent {
	char *name;    /* name of ent */
	ENT_TYPE type; /* type of ent */
	int color;     /* color of char */

	SDL_Texture *img;
	SDL_Rect src;  /* ent position on spirte sheet */
	SDL_Rect dst;  /* ent position */

	int direc;     /* direction ent is floating */
	int rot;       /* rotation of ent */
	int look;      /* direction ent is looking*/
	int speed;     /* how fast entity ai can move */

	bool isdead;   /* set to true after entity dies */
	int isloaded;  /* if gun is loaded */

	struct _Keys keys; /* player keys */

};

/* game.c: handles main game code launched in main */
bool iv_init(void);          /* iv_init: start intervoid */
void game_loop(SDL_Event e); /* game_loop: main loop of game */
void iv_cleanup(void);       /* iv_cleanup: close intervoid */

/* util.c: random universal functions */
SDL_Texture *load_img(char *path);                  /* returns loaded png found at path */
void draw_text(char *str, SDL_Color color); /*  */
bool dst_collision(SDL_Rect dst_1, SDL_Rect dst_2); /* dst_collision: returns true if dst_1 and dst_2 are inside of each other */
void add_degrees(int *old, int diff);               /* add_degrees: adds diff to old, checks to make sure it is still in range */

/* map.c: maps functions */
void init_level(Level *l);  /* init_level: populate Level l with data, right now it is hard coded in */
char get_map(int x, int y); /* get_map: return block char at x and y */

/* ent.c: functions working with entities */
bool init_ent(int count);                  /* init_ent: copies values from player_t[] to player[] */
void init_bullet(SDL_Rect dst, int direc); /* init_bullet: populates an entity as a bullet */
void change_tex(Ent *e, int num);          /* change_tex: move selected texture to other textures around the sprite sheet */
void draw_ent(Ent e);                      /* draw entity e to screen, if alive */
void bullet_run(Ent *e);                   /* bullet_run: bullet code */
void player_run(SDL_Event event, Ent *e);  /* player_run: player code */

/* global vars */
SDL_Window *iv_win;
SDL_Renderer *iv_ren;
TTF_Font *font;

SDL_Texture *nullimg;

Ent ent[MAX_ENTS];
int entqty;
int playerqty;
Game iv_game;

#endif
// vim: set shiftwidth=8 tabstop=8:
