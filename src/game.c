#include "intervoid.h"

#define SDL_ERROR() { \
	printf("SDL Error: %s\n", SDL_GetError()); \
	return false; \
}

/* iv_init: start intervoid */
bool iv_init(void) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) SDL_ERROR()

	iv_win= NULL;
	iv_win = SDL_CreateWindow("intervoid", 100, 100, MAX_X*ZOOM, MAX_Y*ZOOM, SDL_WINDOW_SHOWN);
	iv_ren = NULL;
	iv_ren = SDL_CreateRenderer(iv_win, -1, SDL_RENDERER_ACCELERATED |
				    SDL_RENDERER_PRESENTVSYNC);

	if (iv_win == NULL || iv_ren == NULL) SDL_ERROR()
	if (TTF_Init() != 0) SDL_ERROR()
	if ((font = TTF_OpenFont("data/FreeMono.ttf", 80)) == NULL) SDL_ERROR()

	SDL_SetRenderDrawColor(iv_ren, 0x20, 0x20, 0x20, 0x20);
	IMG_Init(IMG_INIT_PNG);

	nullimg = load_img("data/null.png");

	iv_game.levelnum = 0;
	init_level(&iv_game.level[0]);
	playerqty = 2;
	init_ent(playerqty);

	return true;
}

/* game_loop: main loop of game */
void game_loop(SDL_Event e) {
	do {
		SDL_RenderClear(iv_ren);

		SDL_RenderCopy(iv_ren, iv_game.level[0].img, NULL, NULL);

		/* draw_text("intervoid", (SDL_Color){ 0, 255, 0 }); */

		for (int i = 0; i < entqty; i++)
			draw_ent(ent[i]);

		SDL_RenderPresent(iv_ren);
		SDL_Delay(1);

		for (int i = 0; i < entqty; i++)
			switch (ent[i].type) {
			case PLAYER: player_run(e, &ent[i]); break;
			case BULLET: bullet_run(&ent[i]); break;
			case BOMB:   break;
			}

		SDL_PollEvent(&e);

	} while (e.key.keysym.sym != SDLK_ESCAPE);
}

/* iv_cleanup: close intervoid */
void iv_cleanup(void) {
	SDL_DestroyWindow(iv_win);
	iv_win= NULL;
	TTF_CloseFont(font);
	TTF_Quit();
	SDL_Quit();

	for (int i = 0; i < entqty; i++) {
		free(ent[i].name);
		SDL_DestroyTexture(ent[i].img);
	}
	for (int i = 0; i < iv_game.levelnum; i++)
		SDL_DestroyTexture(iv_game.level[i].img);
}

// vim: set shiftwidth=8 tabstop=8:
